<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@Home');
Route::get('/register', 'AuthController@regist');
Route::get('/welcome', 'AuthController@welcome');
Route::post('/post','AuthController@welcome');
Route::get('/table', 'AuthController@table');

Route::get('/data-tables' , function(){
    return view('data-tables');
});

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast','CastController@store');  /* <<<< Menyimpan Ke DB table cast */
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
Route::get('/cast', 'CastController@index');

