@extends('master')

@section('judul')
    Data Cast
@endsection

@section('content')
<div class="container-fluid">

<a href="/cast/create" class="btn btn-primary mt-3">Tambah Data Cast</a>

<table class="table mt-3">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col" class="text-center">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $index=>$item)
     <tr>
        <td>{{ $index + 1 }}</td>
        <td>{{ $item->nama }}</td>
        <td>{{ $item->umur }}</td>
        <td>{{ $item->bio }}</td>
        <td>
            <form action="/cast/{{ $item->id }}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{ $item->id }}" class="btn btn-primary ">Detail</a>
                <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning ">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
     </tr>
        @empty
            <h1>Data Sedang Kosong</h1>
        @endforelse
        
    </tbody>
  </table>
</div>
@endsection