@extends('master')

@section('judul')
    edit Data Cast
@endsection

@section('content')
<div class="container-fluid ml-4">
    <form class="mt-4" action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group w-50">
          <label>Nama</label>
          <input type="text" class="form-control"  aria-describedby="emailHelp" name="nama" id="nama" value="{{ $cast->nama }}">
          @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
         @enderror
        </div>
        <div class="form-group w-50">
          <label>Umur</label>
          <input type="text" class="form-control"  name="umur" id="umur" value="{{ $cast->umur }}">
          @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
         @enderror
        </div>
        <div class="form-group w-50">
            <label>Bio</label>
            <textarea type="text-area" class="form-control" name="bio">{{ $cast->bio }}</textarea>
          </div>
        <button type="submit" class="btn btn-primary mb-4">Submit</button>
      </form>
    </div>
@endsection