@extends('master')
@section('judul')
    Input Cast Data
@endsection

@section('content')
<div class="container-fluid ml-4">
<form class="mt-4" action="/cast" method="POST">
    @csrf
    <div class="form-group w-50">
      <label>Nama</label>
      <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Nama" name="nama" id="nama">
      @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
     @enderror
    </div>
    <div class="form-group w-50">
      <label>Umur</label>
      <input type="text" class="form-control" placeholder="Umur" name="umur" id="umur">
      @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
     @enderror
    </div>
    <div class="form-group w-50">
        <label>Bio</label>
        <textarea type="text-area" class="form-control" name="bio"></textarea>
      </div>
    <button type="submit" class="btn btn-primary mb-4">Submit</button>
  </form>
</div>
@endsection