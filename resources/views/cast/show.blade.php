@extends('master')

@section('judul')
    Detail cast data
@endsection

@section('content')
<div class="container-fluid ml-4 my-4">
    <h1>{{ $cast->nama }}</h1>
    <p>Umur : {{ $cast->umur }}</p>
    <p>Bio :</p>
    <p>{{ $cast->bio }}</p>
</div>
@endsection